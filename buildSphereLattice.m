function buildSphereLattice(plg_path)
% build a custom spherical lattice structure for SLM

%% setup
% load the json file that specifies how to build everything
spec = jsondecode(fileread('spec.json'));
spec.general.path = plg_path;

addpath(plg_path);
addpath('__functions__');

%% build segments %%
% also required in full build section
[segment_names,segment_data] = segmentRetrieve(spec);

% only required if rebuilding segment, see json for setting
if spec.build_options.segments
    for inc = 1:length(segment_names)
        buildSection(segment_names{inc},segment_data{inc},spec.general); 
        makeImage(segment_names{inc});
    end
end

%% support %%

% only required if rebuilding support, see json for setting
if spec.build_options.support
    buildSupport(spec.support,spec.general); 
    makeImage(spec.support.name);
end

%% build core sections %%
% also required in full build section
names = fieldnames(spec.core);
names(1) = [];
[core_names,core_data] = coreRetrieve(names,spec);

% only required if rebuilding the core, see json for setting
if spec.build_options.core
    g_core = spec.core.general;
    g_all = spec.general;
    for inc = 1:length(core_names)
        buildCore(core_names{inc},core_data{inc},g_all,g_core); 
        makeImage(core_names{inc});
    end
end

%% full build section
if spec.build_options.core || spec.build_options.segments || spec.build_options.support
    name_list = [segment_names;core_names;spec.support.name];
    combineSections(name_list,spec.file_name.custom_segment,spec.file_name.custom_full,spec.general);
    
    %% %% plot manufacturability %% %%
    plotManu(spec.file_name.custom_segment);
    
    %% %% make stl file %% %%
    makeStl(spec.file_name.custom_segment,spec.file_name.stl_segment,spec.general);
    makeStl(spec.file_name.custom_full,spec.file_name.stl_full,spec.general);
end

%% cleanup
rmpath(plg_path);
rmpath('__functions__');
end % buildSphereLattice