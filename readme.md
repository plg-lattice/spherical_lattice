# Making of a spherical lattice

The lattice will consist of the following sections as measured from the horizontal plane (both above and below):

* 90 - 72 the high density cap section `c_70_90.custom`.
* 72 - 45 the low density cap section `c_50_70.custom`.
* 45 - 0 the perimeter section `p_50_-50.custom`.
* Additionally cap sections will have an internal support added.
    * `s_70_90.custom`
    * `s_50_70.custom`

1. To speed up development and aid in visualisation only one segment of the rotation is built and saved to a custom file.
2. All the segments are built individually and saved as custom files.
3. Manufacturing support pins will be added to the cap sections and generated in isolation.
5. The combined segment file is then revolved about the z axis and saved to, `lattice_sphere.custom`.
6. The final custom file is then saved as a stl file `lattice_sphere.stl`. The stl of just the segment is also saved out, `lattice_sphere_seg.stl`.

The overall goals for the lattice are

* ~200mm diameter.
* A single inner and outer layer.
* 30 replications around z.
* approx 20 units from -90 to 90, the density is not constant.
* Mostly air.
* Strut diameter 0.7mm.

```matlab
plg_path = '../PLG';
buildSphereLattice(plg_path);
```

[TOC]

## Caps

The high density cap will be made from bcc and xRods. There will be 4 unit replications from 70->90 degrees.

![c_70_90](images/cap_1_70_90.png)

A second less dense cap section from 70->50 degrees. This section is the critical part in terms of manufacturability. To address this critical component a custom unit cell consisting of y-normal face crosses and only one diagonal in the x-normal cross as the other is at an unsuitable incline. The custom unit cell is available in `mid_cap.custom`.

![c_50_70](images/cap_2_50_70.png)

### Support

The support pins are generated separately. This is useful as there is some errant support pin generation which must be removed. Support pins are added that have a diameter of 0.7mm.

![Supports](images/support.png)

## Sides

Sides consist of a layer of BCC units transformed into polar coordinate system.

![p_50_-50](images/side_-50_50.png)



## Core

The core consists of a distorted sphere that roughly replicates the outer sections and has a diameter is 0.7mm. The cap sections of the core attaches to the two outer cap sections. The core also has a side section but this does not connect to the outer side section.

![s_70_90_2](images/core_1_70_90.png)

![s_50_70](images/core_2_50_70.png)

![s_50_70](images/core_3_-50_50.png)

## All combined

Once all the parts of a single segment are built they are combined and the manufacturability metrics are plotted. Line thickness is proportional to strut diameter.

| ![manufacturability](images/manufacturability.png) |
| -------------------------------------------------- |
| ![manufacturability](images/incline.png)           |
| ![manufacturability](images/dx.png)                |
| ![manufacturability](images/diameter.png)          |

