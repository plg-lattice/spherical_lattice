function [name,core] = coreRetrieve(names,spec)
% from the global json data retrienve the list of file names and segments

if isempty(names)
    name = [];
    core = [];
    return;
end
c_name = names{1};
names(1) = [];
segment_names = fieldnames(spec.segments);

c_core = spec.core.(c_name);
test = strcmp(c_core.derived,segment_names);
segment = spec.segments.(segment_names{test});
c_core.seg = segment.seg;
c_core.reps = segment.reps;
c_core.mirror = segment.mirror;
spec.core = rmfield(spec.core,c_name);
c_name = {sprintf('%s_%d_%d.custom',c_name,c_core.seg(1),c_core.seg(2))};

% recurse
[f_name,f_core] = coreRetrieve(names,spec);
name = [c_name;f_name];
core = [{c_core},f_core];
end % coreRetrieve