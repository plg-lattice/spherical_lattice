function combineSections(file_names,file_out_seg,file_out_full,g)
% combine multiple segments and revolve to generate the entire sphere.
n = length(file_names);
obj = PLG(['outputs/',file_names{1}]);
for inc = 2:n
    obj = obj + PLG(['outputs/',file_names{inc}]);
end
obj = cleanLattice(obj,g.tolerance);
save(obj,['outputs/',file_out_seg]); % segment.custom

% revolve to get whole sphere
rot = 360/g.reps.tangential;
new = obj;
all = obj;
for inc = 1:g.reps.tangential
    new = rotate(new,0,0,rot);
    all = all + new;
end
all = cleanLattice(all,g.tolerance);
save(all,['outputs/',file_out_full]);
end % combineSections