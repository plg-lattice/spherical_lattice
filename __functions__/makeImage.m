function makeImage(f_name)
% saves a figure at a standard view and size
obj = plotPLG(['outputs/',f_name]);
obj = setStyle(obj,'manufacturability','processMap.csv');

[~,f_name] = fileparts(f_name);
obj.a.View = [0,0];
print(obj.f,['images/',f_name,'.png'],'-dpng','-r300');
close(obj.f);
end % makeImage