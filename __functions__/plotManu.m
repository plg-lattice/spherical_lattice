function plotManu(f_name)
% save manu images

% setup
combo = plotPLG(['outputs/',f_name]);
combo = setStrutWidth(combo,'diameter',4);
combo.a.View = [0,0];

% diameter (default)
print(combo.f,'images/diameter.png','-r180','-dpng');

% dx
% combo = setStyle(combo,'dx',[1,5,10,20,40]);
combo = setStyle(combo,'dx',[5:15,20,40]);
print(combo.f,'images/dx.png','-r180','-dpng');

% incline
combo = setStyle(combo,'incline',[10,20,30,45,55,90]);
print(combo.f,'images/incline.png','-r180','-dpng');

% manufacturable
combo = setStyle(combo,'manufacturability','processMap.csv');
print(combo.f,'images/manufacturability.png','-r180','-dpng');

close(combo.f);
end % plotManu(f_name);