function makeStl(file_name_in,file_name_out,g)
obj = custom2stl(['outputs/',file_name_in]);
obj = set(obj,'resolution',g.resolution);
obj = set(obj,'sphereResolution',g.resolution);
obj = set(obj,'sphereAddition',true);
obj = set(obj,'baseFlat',true);
save(obj,['outputs/',file_name_out]);
end % makeStl