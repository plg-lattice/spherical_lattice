function buildSupport(sup,g)
% build some support pins

% combine the files which need to be supported
f_namer = @(s,n,m) sprintf('outputs/%s*.custom',s);
n = length(sup.parts);
s = dir(f_namer(sup.parts{1}));
obj = PLG([s.folder,filesep,s.name]);
if n>1
    for inc = 2:n
        s = dir(f_namer(sup.parts{inc}));
        obj = obj + PLG([s.folder,filesep,s.name]);
    end
end
obj = cleanLattice(obj,g.tolerance);
save(obj,['outputs/',sup.name]);

% load the combined file and add supports
obj = addSupport(['outputs/',sup.name],sup.diameter,sup.diameter,sup.incline,sup.search);
obj = padSupport(obj,sup.pad,sup.diameter,sup.diameter);
obj = cleanLattice(obj);
% Remove points that are not connected to the outside of the sphere
s1 = find(obj.support_vert_type==1);
test = arrayfun(@(x) any(x==s1),obj.struts(:,1));
s2 = obj.struts(test,2);

v2 = obj.vertices(s2,:);

% v1 = obj.vertices(s1,:);
% plot(obj);
% s = scatter3(v1(:,1),v1(:,2),v1(:,3));
% s.MarkerFaceColor = 'b';
% s = scatter3(v2(:,1),v2(:,2),v2(:,3));
% s.MarkerFaceColor = 'g';

r = sqrt(sum(v2.^2,2));
I = abs(r-g.R)<g.tolerance;
s1(I) = [];
obj = removePin(obj,s1);

save(obj,['outputs/',sup.name],'support');
end % buildSupport