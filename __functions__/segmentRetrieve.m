function [name,segment] = segmentRetrieve(spec)
% from the global json data retrienve the list of file names and segments

c_name = fieldnames(spec.segments);
if isempty(c_name)
    name = [];
    segment = [];
    return;
end
c_name = c_name{1};
c_segment = spec.segments.(c_name);
spec.segments = rmfield(spec.segments,c_name);
c_name = {sprintf('%s_%d_%d.custom',c_name,c_segment.seg(1),c_segment.seg(2))};

% recurse
[f_name,f_segment] = segmentRetrieve(spec);
name = [c_name;f_name];
segment = [{c_segment},f_segment];
end % segmentRetrieve