function obj = buildSection(f_name,spec,g)
% generate a portion of the sperical lattice
obj = PLG();
obj = set(obj,'strutDiameter',g.diameter);
obj = set(obj,'sphereDiameter',g.diameter);
obj = set(obj,'sphereAddition',true);
r = spec.seg*pi/180; % convert to radians
obj = set(obj,'unitSize',[g.unit_size,2*pi/(g.reps.tangential),(r(2)-r(1))/spec.reps]);
obj = set(obj,'replications',[g.reps.radial,1,spec.reps]);

% unit cell
[~,f,ext] = fileparts(spec.unit{1});
if ~isempty(ext) || strcmp(ext,'.custom')
    p = [g.path,'/unitCell'];
    addpath(p);
    pp = [p,'/',f,'.xml'];
    custom2xml(spec.unit{1},pp);
    rmpath(p);
    [~,s,~] = fileparts(pp);
    spec.unit = {s};
end
obj = defineUnit(obj,spec.unit);
% replicate and translate
obj = cellReplication(obj); % requires the replications to be set
obj = translate(obj,...
    g.R + g.unit_size*(1/2-g.reps.radial), pi/g.reps.tangential,r(1) + 1/2*(r(2)-r(1))/spec.reps);
obj = cartesian2polar(obj);

% duplicate the cap
if spec.mirror
   obj2 = rotate(obj,180,0,0);
   obj = obj+obj2;
end
obj = cleanLattice(obj,g.tolerance); % remove duplicate points and merge close points
save(obj,['outputs/',f_name]);

if ~isempty(ext) || strcmp(ext,'.custom')
    delete(pp)
end
end % buildSection