function obj = buildCore(f_name,spec,g_all,g_core)
% Build a core section that supports the outer layer.
%   1. Create points that match the inside of the outter shell.
%   2. Create an inner lattice layer.
%       2.1 Identify the outter layer of points.
%       2.2 Scale the lattice in z.
%   3. Join outter shell points to the identified points.
%       3.1 save points to a custom.
%       3.2 Load as a custom.
%   4. Join this object to the points in 2.
%   5. clean and save.
% 1. Create points that match the inside of the outter shell.
if spec.connect
    ID = g_all.R - g_all.unit_size*g_all.reps.radial;
    theta = [0,2*pi/g_all.reps.tangential];
    r = spec.seg*pi/180; % convert to radians
    omega = r(1):(r(2)-r(1))/spec.reps:r(2);
    [theta,omega] = meshgrid(theta,omega);
    omega = omega(:);
    theta = theta(:);
    points = [...
        ID.*cos(theta).*cos(omega),...
        ID.*sin(theta).*cos(omega),...
        ID.*sin(omega)...
        ];
    points = uniquetol(points,g_all.tolerance,'ByRows',true);
end % spec.connect

% 2. Create an inner lattice layer.
gg = g_core;
gg.reps.radial = g_core.radial;
gg.reps.tangential = g_all.reps.tangential;
gg.tolerance = g_all.tolerance;
gg.diameter = g_core.diameter;
s = spec;
s.mirror = false;
obj = buildSection(f_name,s,gg);
% 2.1 Identify the outter layer of points.
r = sqrt(sum(obj.vertices.^2,2));
I = abs(r-g_core.R)<1e-3;
% 2.2 Scale the lattice in z.
obj = scale(obj,g_core.scaler(1),g_core.scaler(2),g_core.scaler(3));
scaled_v = unique(obj.vertices(I,:),'rows');

% 3. Join outter shell points to the identified points.
f_name = ['outputs/',f_name];
if spec.connect
    n = size(points,1);
    connections = [1:n;(1:n)+n]';
    % 3.1 save the data to a .custom file
    dlmwrite(f_name,2*n);
    dlmwrite(f_name,n,'-append');
    points = [scaled_v;points];
    data = [points,ones(2*n,1)*g_core.diameter];
    dlmwrite(f_name,data,'-append');
    data = [connections,ones(n,1)*g_core.diameter];
    dlmwrite(f_name,data,'-append');
    % 3.2 load in a plg object
    obj_join = PLG(f_name);
    % 4. Join the objects
    obj = obj + obj_join;
end % if spec.connect
obj = cleanLattice(obj,g_all.tolerance);

% 5. save the objects
if spec.mirror
    obj2 = rotate(obj,180,0,0);
    obj = obj+obj2;
end
save(obj,f_name);
end % buildCoreSection